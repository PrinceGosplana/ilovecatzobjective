//
//  ShrinkDismissAnimationController.h
//  ILoveCatz
//
//  Created by Александр Исаев on 11.03.15.
//  Copyright (c) 2015 com.razeware. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShrinkDismissAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

@end
