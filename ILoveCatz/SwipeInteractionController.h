//
//  SwipeInteractionController.h
//  ILoveCatz
//
//  Created by Александр Исаев on 11.03.15.
//  Copyright (c) 2015 com.razeware. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SwipeInteractionController : UIPercentDrivenInteractiveTransition

- (void) wireToViewController: (UIViewController *) viewController;

@property (nonatomic, assign) BOOL interactionInProgress;

@end
